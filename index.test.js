//bibliotecas utilizadas
const test = require('tape')
const supertest = require('supertest')
const app = require('./index')

//teste do endpoint
test('GET Sucesso', (t) => {
    supertest(app)
      .get('/juntar?palavra1=dev&palavra2=ops')      
      .expect(200)
      .end((err, res) =>{
        t.error(err, 'Sem erros')
        t.assert(res.body.resultado === 'devops', "ok")
        t.end()  
      })
})

test('GET Tratamento palavra indesejada', (t) => {
    supertest(app)
      .get('/juntar?palavra1=dev&palavra2=ops')      
      .expect(200)
      .end((err, res) =>{
        t.error(err, 'Sem erros')
        t.assert(res.body.resultado === 'devops', "ok")
        t.end()  
      })
})
